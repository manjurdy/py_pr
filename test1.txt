for i in range(0,30):
...    if i % 5 == 0:
...       print i
...       print "fizz"
...    elif i % 3 == 0:
...       print i
...       print "bizz"
...    elif i % 5 == 0 or i % 3 == 0:
...       print i
...       print "bizz"
...       print "fizz"
...
0
fizz
3
bizz
5
fizz
6
bizz
9
bizz
10
fizz
12
bizz
15
fizz
18
bizz
20
fizz
21
bizz
24
bizz
25
fizz
27
bizz
>>> for i in range(0,30):
...   if i % 5 == 0:
...      print i
...      print "fizz"
...   elif i % 3 == 0:
...      print i
...      print "bizz"
...   elif i % 5 == 0 and i % 3 == 0:
...      print i
...      print "f and b"
...
0
fizz
3
bizz
5
fizz
6
bizz
9
bizz
10
fizz
12
bizz
15
fizz
18
bizz
20
fizz